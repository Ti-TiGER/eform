import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import TextField from "@mui/material/TextField";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Avatar from "@mui/material/Avatar";
import { spacing } from "@mui/system";

function Login() {
  return (
    <Box
      component="form"
      sx={{
        "& .MuiTextField-root": { m: 1, width: "25ch" },
      }}
      noValidate
      autoComplete="off"
    >
      <AppBar position="static">
        <Toolbar>
          <IconButton>
            <ArrowBackIosIcon
              size="large"
              edge="start"
              color="#fff"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </ArrowBackIosIcon>
          </IconButton>
          <Typography
            align="center"
            variant="h6"
            component="div"
            sx={{ flexGrow: 1 }}
          >
            รายงานผลตรวจโรงงาน
          </Typography>
        </Toolbar>
      </AppBar>
      <Paper sx={{ maxWidth: 400, my: 1, mx: "auto", p: 2 }}>
        <Typography
          align="center"
          variant="h6"
          component="div"
          sx={{ flexGrow: 1 }}
        >
          ข้อมูลทั่วไป
        </Typography>

        <Grid container wrap="nowrap" spacing={2}>
          <Typography>ชื่อโรงงาน</Typography>
          <TextField
            required
            id="outlined-required"
            label="ชื่อโรงงาน"
            placeholder="ระบุข้อมูล"
          />
        </Grid>
      </Paper>
    </Box>
  );
}

export default Login;
