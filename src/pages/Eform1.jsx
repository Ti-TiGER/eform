import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
  TextField,
  Paper,
  AppBar,
  CssBaseline,
  Toolbar,
  Container,
  Button,
} from "@mui/material";
import { Box } from "@mui/system";
import IconButton from "@mui/material/IconButton";
import LinearProgress from "@mui/material/LinearProgress";
import MenuItem from "@mui/material/MenuItem";
import Autocomplete from "@mui/material/Autocomplete";
import Select from "@mui/material/Select";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import MenuIcon from "@mui/icons-material/Menu";

const Eform1 = () => {
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    // eslint-disable-next-line no-console
    console.log({
      email: data.get("email"),
      password: data.get("password"),
    });
    window.location = "/eform2";
  };

  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    setCurrentDate(date + "/" + month + "/" + year);
  }, []);

  const โรงงานจำพวกที่ = [
    { label: "โรงงานจำพวกที่ 1" },
    { label: "โรงงานจำพวกที่ 2" },
    { label: "โรงงานจำพวกที่ 3" },
    { label: "โรงงานจำพวกที่ 4" },
  ];

  const อยู่ในผังเมืองสี่ = [
    { label: "อยู่ในผังเมืองสี่ 1" },
    { label: "อยู่ในผังเมืองสี่ 2" },
    { label: "อยู่ในผังเมืองสี่ 3" },
    { label: "อยู่ในผังเมืองสี่ 4" },
  ];

  const ทำเลโดยรอบเป็น = [
    { label: "ทำเลโดยรอบเป็นทะเล " },
    { label: "ทำเลโดยรอบเป็นชุมชน " },
    { label: "ทำเลโดยรอบเป็นสนามบิน " },
    { label: "ทำเลโดยรอบเป็นโรงงาน " },
    { label: "ทำเลโดยรอบเป็นถนน " },
    { label: "ทำเลโดยรอบเป็นคลอง " },
  ];

  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <>
      <CssBaseline />
      <AppBar
        position="relative"
        sx={{
          backgroundColor: "#fff",
        }}
      >
        <Toolbar>
          <IconButton>
            <ArrowBackIosIcon
              size="large"
              edge="start"
              color="#fff"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </ArrowBackIosIcon>
          </IconButton>
          <Typography
            align="center"
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, color: "black" }}
          >
            รายงานผลตรวจโรงงาน
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        <Paper sx={{ maxWidth: 400, my: 1, mx: "auto", p: 2 }}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
            }}
          >
            <Typography>วันที่ {currentDate}</Typography>

            <Typography sx={{ pl: 20 }}>ขั้นตอน 1 / 4</Typography>
          </Box>

          <Box sx={{ width: "100%", alignSelf: "flex-end" }}>
            <LinearProgress color="success" variant="determinate" value={25} />
          </Box>
        </Paper>

        <Paper sx={{ maxWidth: 400, my: 1, mx: "auto", p: 2 }}>
          <Box sx={{ mb: 1 }}>
            <Typography variant="h6" gutterBottom>
              ข้อมูลทั่วไป
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>ชื่อโรงงาน</Typography>
            <TextField placeholder="ระบุข้อมูล" sx={{ ml: 9 }}></TextField>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>ประกอบกิจการ</Typography>
            <TextField placeholder="ระบุข้อมูล" sx={{ ml: 5.5 }}></TextField>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>เบอร์โทรศัพท์</Typography>
            <TextField placeholder="ระบุข้อมูล" sx={{ ml: 6 }}></TextField>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>โรงงานจำพวกที่</Typography>

            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={โรงงานจำพวกที่}
              sx={{ width: 256, pl: 4.5 }}
              renderInput={(params) => (
                <TextField {...params} label="ระบุข้อมูล" />
              )}
            />
          </Box>

          <Box>
            <Typography variant="subtitle1" gutterBottom>
              ชำระค่าธรรมเนียมรายปี
            </Typography>
            <RadioGroup
              row
              aria-label="ชำระค่าธรรมเนียมรายปี"
              name="row-radio-buttons-group"
            >
              <FormControlLabel
                value="ครบถ้วน"
                control={<Radio color="success" />}
                label="ครบถ้วน"
              />
              <FormControlLabel
                value="ไม่ครบถ้วน"
                control={<Radio color="error" />}
                label="ไม่ครบถ้วน"
                sx={{ pl: 10 }}
              />
            </RadioGroup>
          </Box>
        </Paper>

        <Paper sx={{ maxWidth: 400, my: 1, mx: "auto", p: 2 }}>
          <Box>
            <Typography variant="subtitle1" gutterBottom>
              ที่ตั้งสภาพแวดล้อม
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>พิกัดที่ตั้ง (UTM)</Typography>
            <TextField placeholder="ระบุพิกัด" sx={{ ml: 3.7 }}></TextField>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>อยู่ในฝังเมืองสี่</Typography>

            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={อยู่ในผังเมืองสี่}
              sx={{ width: 265, pl: 5.5 }}
              renderInput={(params) => (
                <TextField {...params} label="ระบุข้อมูล" />
              )}
            />
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              flexGrow: 1,
              pb: 2,
            }}
          >
            <Typography>ทำเลโดยรอบเป็น</Typography>

            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={ทำเลโดยรอบเป็น}
              sx={{ width: 247, pl: 3.3 }}
              renderInput={(params) => (
                <TextField {...params} label="ระบุข้อมูล" />
              )}
            />
          </Box>

          <Box>
            <Typography variant="subtitle1" gutterBottom>
              ชำระค่าธรรมเนียมรายปี
            </Typography>
            <RadioGroup
              row
              aria-label="ชำระค่าธรรมเนียมรายปี"
              name="row-radio-buttons-group"
            >
              <FormControlLabel
                value="ขัด"
                control={<Radio color="success" />}
                label="ขัด"
              />
              <FormControlLabel
                value="ไม่ขัด"
                control={<Radio color="error" />}
                label="ไม่ขัด"
                sx={{ pl: 10 }}
              />
            </RadioGroup>
          </Box>
        </Paper>

        <Paper sx={{ maxWidth: 400, my: 1, mx: "auto", p: 2 }}>
          <Box onSubmit={handleSubmit}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{
                mx: "auto",
                bgcolor: "#004e65",
                color: "#fff",
                p: 1,
                borderRadius: 1,
                textAlign: "center",
              }}
            >
              ต่อไป
            </Button>
          </Box>
        </Paper>
      </main>
    </>
  );
};

export default Eform1;
