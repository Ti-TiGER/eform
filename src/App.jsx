import Login from "./pages/Login";
import Eform1 from "./pages/Eform1";
import Eform2 from "./pages/Eform2";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login">
          <Login />
        </Route>

        <Route path="/eform1">
          <Eform1 />
        </Route>

        <Route path="/eform2">
          <Eform2 />
        </Route>

        <Route path="/">
          <Eform1 />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
